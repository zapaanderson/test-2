var express = require('express');
var router = express.Router();
var util = require('../utility/util');



router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/time', util.validateForm, function(req, res, next) {

    //récupération de la configuration des bornes
    var trafficLights = req.body.trafficLights.split(';');
    //récupération de la vitesse initiale
    var speed = req.body.speed;

    //temps à parcourir pour atteindre la première borne
    var timeAtFirstLight = util.defaultDistance / speed;


    //distance total parcourue du point initial à la derniere borne
    var result = 0;
    for (var i = 0; i < trafficLights.length; i++) {

        if (i == 0) {
            result = parseFloat(util.timeToPassALight(trafficLights[0], timeAtFirstLight));
        } else {
            result = parseFloat(util.timeToPassALight(trafficLights[i], result + timeAtFirstLight));
        }

    }


    res.render('result', { result: Math.round((result + timeAtFirstLight) * 100) / 100 });

});

module.exports = router;