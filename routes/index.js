var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {

    var errorForm = req.session.errorForm;
    //validation du formulaire
    if (errorForm == 401) {
        req.session.destroy()
        res.render('index', { error: 'Verifiez si vos champs sont remplis correctement' });
    } else {
        res.render('index', { error: false });
    }

});

module.exports = router;