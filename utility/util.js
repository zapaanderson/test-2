exports.defaultDistance = 150;


//timeChangeLight: durée du changement de feux sur une borne
//timeAtNextLight: durée pour arriver à la prochine borne 
exports.timeToPassALight = function(timeChangeLight, timeAtNextLight) {

    if (timeAtNextLight < timeChangeLight) {

        return timeAtNextLight;

    } else {
        var stateTheLight = Math.trunc(timeAtNextLight / timeChangeLight);

        if (stateTheLight % 2 != 0) {
            //ici le feux est rouge lorsque le conducteur est à ce feux

            //whenLightsIsgreen: le temps dont le feux passera au vert juste après l'arrêt du conducteur
            var whenLightsIsgreen = (Math.trunc(timeAtNextLight / timeChangeLight) + 1) * timeChangeLight;

            //durée d'atttente du conducteur au feux rouge
            var waitingTime = whenLightsIsgreen - timeAtNextLight;

            return timeAtNextLight + waitingTime;

        }

        return timeAtNextLight;

    }

}



exports.validateForm = function(req, res, next) {

    //récupération de la configuration des bornes
    var trafficLights = req.body.trafficLights.split(';');
    //récupération de la vitesse initiale
    var speed = req.body.speed;

    for (var i = 0; i < trafficLights.length; i++) {
        if (isNaN(trafficLights[i])) {
            req.session.errorForm = 401;
            res.redirect('/');
        }
    }

    if (isNaN(speed)) {
        req.session.errorForm = 401;
        res.redirect('/');
    }

    next();


}